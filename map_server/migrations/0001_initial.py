# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Map'
        db.create_table('map_server_map', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='New Map', unique=True, max_length=64)),
            ('center_latitude', self.gf('django.db.models.fields.DecimalField')(default=54.7, max_digits=10, decimal_places=7)),
            ('center_longitude', self.gf('django.db.models.fields.DecimalField')(default=-2.0, max_digits=10, decimal_places=7)),
            ('initial_zoom', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=6)),
            ('tile_set', self.gf('django.db.models.fields.CharField')(default='google.maps.MapTypeId.TERRAIN', max_length=32)),
            ('specific_js', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('specific_css', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('specific_html', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('shareable_url_root', self.gf('django.db.models.fields.CharField')(max_length=512, blank=True)),
            ('published_url', self.gf('django.db.models.fields.CharField')(max_length=512, blank=True)),
            ('accept_submissions', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('submission_header_html', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('map_server', ['Map'])

        # Adding M2M table for field layers on 'Map'
        db.create_table('map_server_map_layers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('map', models.ForeignKey(orm['map_server.map'], null=False)),
            ('layer', models.ForeignKey(orm['map_server.layer'], null=False))
        ))
        db.create_unique('map_server_map_layers', ['map_id', 'layer_id'])

        # Adding model 'Layer'
        db.create_table('map_server_layer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='New Layer', max_length=64)),
            ('visible', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('legend_icon', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='layer_legend_icon', null=True, to=orm['map_server.MarkerIcon'])),
            ('should_cluster', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('cluster_icon', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='layer_cluster_icons', null=True, to=orm['map_server.MarkerIcon'])),
            ('cluster_text_color', self.gf('django.db.models.fields.CharField')(default='000000', max_length=6)),
            ('z_index', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal('map_server', ['Layer'])

        # Adding M2M table for field datasets on 'Layer'
        db.create_table('map_server_layer_datasets', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('layer', models.ForeignKey(orm['map_server.layer'], null=False)),
            ('layerdataset', models.ForeignKey(orm['map_server.layerdataset'], null=False))
        ))
        db.create_unique('map_server_layer_datasets', ['layer_id', 'layerdataset_id'])

        # Adding M2M table for field marker_icons on 'Layer'
        db.create_table('map_server_layer_marker_icons', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('layer', models.ForeignKey(orm['map_server.layer'], null=False)),
            ('markericon', models.ForeignKey(orm['map_server.markericon'], null=False))
        ))
        db.create_unique('map_server_layer_marker_icons', ['layer_id', 'markericon_id'])

        # Adding model 'LayerDataset'
        db.create_table('map_server_layerdataset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='New Dataset', max_length=64)),
            ('source_location', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('source_type', self.gf('django.db.models.fields.CharField')(default='fusion', max_length=16)),
            ('render_location', self.gf('django.db.models.fields.CharField')(default='local', max_length=16)),
            ('latitude_field', self.gf('django.db.models.fields.CharField')(default='lat', max_length=64)),
            ('longitude_field', self.gf('django.db.models.fields.CharField')(default='lng', max_length=64)),
            ('decimal_precision', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=3)),
            ('field_override', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('fusion_styles', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('popup_html', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('parser', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('max_result_count', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('render_frequency_in_minutes', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=30)),
            ('last_render_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('render_status', self.gf('django.db.models.fields.CharField')(default='waiting', max_length=16)),
        ))
        db.send_create_signal('map_server', ['LayerDataset'])

        # Adding model 'MarkerIcon'
        db.create_table('map_server_markericon', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('icon', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('map_server', ['MarkerIcon'])

        # Adding model 'UserSubmission'
        db.create_table('map_server_usersubmission', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('map', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['map_server.Map'])),
            ('photo', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('postcode', self.gf('django.db.models.fields.CharField')(max_length=8, blank=True)),
            ('latitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('longitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('firstname', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('lastname', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('data_protection_can_send', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('color', self.gf('django.db.models.fields.CharField')(max_length=6, blank=True)),
            ('uid', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('approval_status', self.gf('django.db.models.fields.CharField')(default='pending', max_length=16)),
            ('approval_email_sent', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('geocoding_attempts', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal('map_server', ['UserSubmission'])

        # Adding model 'ExclusionBox'
        db.create_table('map_server_exclusionbox', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lat_1', self.gf('django.db.models.fields.DecimalField')(max_digits=7, decimal_places=5)),
            ('lon_1', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=5)),
            ('lat_2', self.gf('django.db.models.fields.DecimalField')(max_digits=7, decimal_places=5)),
            ('lon_2', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=5)),
        ))
        db.send_create_signal('map_server', ['ExclusionBox'])


    def backwards(self, orm):
        # Deleting model 'Map'
        db.delete_table('map_server_map')

        # Removing M2M table for field layers on 'Map'
        db.delete_table('map_server_map_layers')

        # Deleting model 'Layer'
        db.delete_table('map_server_layer')

        # Removing M2M table for field datasets on 'Layer'
        db.delete_table('map_server_layer_datasets')

        # Removing M2M table for field marker_icons on 'Layer'
        db.delete_table('map_server_layer_marker_icons')

        # Deleting model 'LayerDataset'
        db.delete_table('map_server_layerdataset')

        # Deleting model 'MarkerIcon'
        db.delete_table('map_server_markericon')

        # Deleting model 'UserSubmission'
        db.delete_table('map_server_usersubmission')

        # Deleting model 'ExclusionBox'
        db.delete_table('map_server_exclusionbox')


    models = {
        'map_server.exclusionbox': {
            'Meta': {'object_name': 'ExclusionBox'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat_1': ('django.db.models.fields.DecimalField', [], {'max_digits': '7', 'decimal_places': '5'}),
            'lat_2': ('django.db.models.fields.DecimalField', [], {'max_digits': '7', 'decimal_places': '5'}),
            'lon_1': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '5'}),
            'lon_2': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '5'})
        },
        'map_server.layer': {
            'Meta': {'ordering': "['id']", 'object_name': 'Layer'},
            'cluster_icon': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'layer_cluster_icons'", 'null': 'True', 'to': "orm['map_server.MarkerIcon']"}),
            'cluster_text_color': ('django.db.models.fields.CharField', [], {'default': "'000000'", 'max_length': '6'}),
            'datasets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['map_server.LayerDataset']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend_icon': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'layer_legend_icon'", 'null': 'True', 'to': "orm['map_server.MarkerIcon']"}),
            'marker_icons': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['map_server.MarkerIcon']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'New Layer'", 'max_length': '64'}),
            'should_cluster': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'z_index': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        'map_server.layerdataset': {
            'Meta': {'object_name': 'LayerDataset'},
            'decimal_precision': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'field_override': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fusion_styles': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_render_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'latitude_field': ('django.db.models.fields.CharField', [], {'default': "'lat'", 'max_length': '64'}),
            'longitude_field': ('django.db.models.fields.CharField', [], {'default': "'lng'", 'max_length': '64'}),
            'max_result_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'New Dataset'", 'max_length': '64'}),
            'parser': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'popup_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'render_frequency_in_minutes': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '30'}),
            'render_location': ('django.db.models.fields.CharField', [], {'default': "'local'", 'max_length': '16'}),
            'render_status': ('django.db.models.fields.CharField', [], {'default': "'waiting'", 'max_length': '16'}),
            'source_location': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'source_type': ('django.db.models.fields.CharField', [], {'default': "'fusion'", 'max_length': '16'})
        },
        'map_server.map': {
            'Meta': {'object_name': 'Map'},
            'accept_submissions': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'center_latitude': ('django.db.models.fields.DecimalField', [], {'default': '54.7', 'max_digits': '10', 'decimal_places': '7'}),
            'center_longitude': ('django.db.models.fields.DecimalField', [], {'default': '-2.0', 'max_digits': '10', 'decimal_places': '7'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initial_zoom': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '6'}),
            'layers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['map_server.Layer']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'New Map'", 'unique': 'True', 'max_length': '64'}),
            'published_url': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'shareable_url_root': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'specific_css': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'specific_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'specific_js': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'submission_header_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'tile_set': ('django.db.models.fields.CharField', [], {'default': "'google.maps.MapTypeId.TERRAIN'", 'max_length': '32'})
        },
        'map_server.mapdatasource': {
            'Meta': {'ordering': "['-dated']", 'object_name': 'MapDataSource', 'db_table': "'action_master_for_maps'", 'managed': 'False'},
            'action_id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'}),
            'dated': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'source_code': ('django.db.models.fields.CharField', [], {'max_length': '31'}),
            'x_coord': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'y_coord': ('django.db.models.fields.CharField', [], {'max_length': '7'})
        },
        'map_server.markericon': {
            'Meta': {'object_name': 'MarkerIcon'},
            'icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'})
        },
        'map_server.usersubmission': {
            'Meta': {'object_name': 'UserSubmission'},
            'approval_email_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'approval_status': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '16'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '6', 'blank': 'True'}),
            'data_protection_can_send': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'firstname': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'geocoding_attempts': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['map_server.Map']"}),
            'photo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'})
        }
    }

    complete_apps = ['map_server']