from rest_framework import renderers

class PlainTextJSONRenderer(renderers.JSONRenderer):
    media_type = 'text/plain'
