'use strict';

/* App Module */

angular.module('mapmaker', ['mapMakerServices', 'ui.bootstrap', 'ngCookies', 'ngUpload']).
  config(['$routeProvider', function($routeProvider) {
	$routeProvider.
	  when('/maps', {templateUrl: '{% url 'mapmaker-partial-map' %}',   controller: MapDetailCtrl}).
	  when('/maps/:mapId', {templateUrl: '{% url 'mapmaker-partial-map' %}',   controller: MapDetailCtrl}).
      otherwise({redirectTo: '/maps'});
	}]).
	directive('ngModelOnblur', function()
	{   // http://stackoverflow.com/questions/11868393/angularjs-inputtext-ngchange-fires-while-the-value-is-changing
		return {
			restrict: 'A',
	        require: 'ngModel',
	        link: function(scope, elm, attr, ngModelCtrl)
			{   if (attr.type === 'radio' || attr.type === 'checkbox') return;
	            elm.unbind('input').unbind('keydown').unbind('change');
	            elm.bind('blur', function()
	            {   scope.$apply(function()
	                {   ngModelCtrl.$setViewValue(elm.val());
	                });
	            });
	        }
	    };
	});


/* Services */

angular.module('mapMakerServices', ['ngResource']).
    factory('MapList', function($resource){return $resource('{% url 'mapmaker-map-list' %}', {}, {});}).
	factory('Map', function($resource){return $resource('/mapmaker/map/:id/', {}, {});}).
	factory('MapPublish', function($resource){return $resource('/mapmaker/map/:id/publish/', {}, {});}).
	factory('LayerList', function($resource){return $resource('{% url 'mapmaker-layer-list' %}', {}, {});}).
	factory('Layer', function($resource){return $resource('/mapmaker/layer/:id/', {}, {});}).
	factory('DatasetList', function($resource){return $resource('{% url 'mapmaker-dataset-list' %}', {}, {});}).
	factory('Dataset', function($resource){return $resource('/mapmaker/dataset/:id/', {}, {});}).
	factory('MarkerIconList', function($resource){return $resource('{% url 'mapmaker-markericon-list' %}', {}, {});}).
	factory('MarkerIcon', function($resource){return $resource('/mapmaker/markericon/:id/', {}, {});});


/* Controllers */

function MapDetailCtrl($scope, $routeParams, $location, $timeout, $cookieStore, Map, MapList, MapPublish, Layer, LayerList, Dataset, DatasetList, MarkerIcon, MarkerIconList)
{   $scope.mapId = $routeParams.mapId;

	$scope.ui_signals = {
		// Consolidate flags into ui_signals.  ng-switch scoping workaround
		mapStatusBadge: 0,
		layerStatusBadge: 0,
		datasetStatusBadge: 0,
		selectedLayer: false,  // Set this as a generic, not just ui signal
		hasUserSubmissionDataset: false
	};

	$scope.firstLoadData = function ()
	{   $scope.maps = MapList.query(function()
		{   // If we're not given a map id to load, check for the last map used otherwise load first map

			if(_.isUndefined($scope.mapId))
			{   if($cookieStore.get("lastMapId"))
					$location.path('/maps/' + $cookieStore.get("lastMapId"));
				else if(_.isArray($scope.maps) && _.size($scope.maps) > 0)
					$location.path('/maps/' + $scope.maps[0].id);
			}

			$scope.map = Map.get({id: $scope.mapId}, function()
			{	$scope.datasets = DatasetList.query();
				$scope.loadLayers();
				setCookie("lastMapId", $scope.mapId);
			});
		});
	};


	$scope.saveAll = function()
	{   _.each($scope.map.layer_objects, function(layer)
		{   if(layer.dataset_objects)
				_.each(layer.dataset_objects, function(dataset){ $scope.saveDataset(dataset) });
			$scope.saveLayer(layer);
		});
		$scope.saveMap(true);
    };


// Init ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$scope.firstLoadData();


// UI Updating /////////////////////////////////////////////////////////////////////////////////////////////////////////
	function setCookie(name, value)
	{   $cookieStore.put(name, value, {expires: oneWeekFromNow()});
		function oneWeekFromNow(){ return new Date(new Date().getTime() + 604800000); }
	}

	function showElement(elementName, status, duration)
	{   if(!status)
			var status = 'success';
		if(!duration)
			var duration = 5000;  // 5 seconds
		$scope.ui_signals[elementName] = status;
		$timeout(function(){ $scope.ui_signals[elementName] = false; }, duration);
	}

	function getLayerObjectFromId(layerId)
	{   if (_.contains($scope.map.layers, layerId))
			return _.find($scope.map.layer_objects, function(layer_objects){ return layer_objects.id == layerId; });
	}

	function displayTabForLayerId(layerId)
	{   if(!layerId && $cookieStore.get("lastLayerId") && _.contains($scope.map.layers, $cookieStore.get("lastLayerId")))
			var layerId = $cookieStore.get("lastLayerId");
		else if(!layerId && $scope.map.layer_objects && _.size($scope.map.layer_objects) > 0)
			var layerId = $scope.map.layer_objects[0].id;

		_.each($scope.map.layer_objects, function(layer){ layer.focused = false; });
		getLayerObjectFromId(layerId).focused = true;
		setCurrentLayer(layerId);
	}

	function setCurrentLayer(layerId)
	{   if(!layerId)
			var layerId = _.find($scope.map.layer_objects, function(layer_objects){ return layer_objects.focused; }).id;
		setCookie("lastLayerId", layerId);
	}

	function getCurrentLayer()
	{   if($cookieStore.get("lastLayerId"))
			 return getLayerObjectFromId($cookieStore.get("lastLayerId"));
		return _.find($scope.map.layer_objects, function(layer_objects){ return layer_objects.focused; })
	}

	function hasUserSubmissionDataset()
	{   var hasUserSubmissionDataset = false;
		hasUserSubmissionDataset = _.find($scope.map.layer_objects, function(layer)
		{   return _.find(layer.dataset_objects, function(dataset){ return dataset.source_type == 'user_submissions' })
		});
		if (hasUserSubmissionDataset)
			hasUserSubmissionDataset = true;

		$scope.ui_signals.hasUserSubmissionDataset = hasUserSubmissionDataset;
		console.log('Dataset user_submissions: ' + hasUserSubmissionDataset);
		return hasUserSubmissionDataset;
	}

// Maps ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	$scope.addNewMap = function ()
	{   var newMap = new MapList({});
		newMap.$save(function(){ $location.path('/maps/' + newMap.id); });
	};

	$scope.saveMap = function(showBadge, preview)
	{   _.omit($scope.map, 'layer_objects').$update({id: $scope.map.id},
		function()
		{   loadMapList();
			showBadge ?  showElement('mapStatusBadge', 'saved') : console.log('Saved Map');
			preview ? $scope.previewModal.isOpen = true : '';
		},
		function()
		{   showElement('mapStatusBadge', 'saveError', 10000);
		});
    };

	$scope.publishMap = function()
	{   $scope.mapPublish = MapPublish.get({id: $scope.mapId}, function()
		{   $scope.mapPublish.$update({id: $scope.mapId}, function()
			{   Map.get({id: $scope.mapId}, function(map)
				{	$scope.map.published_url = map.published_url
					showElement('mapStatusBadge', 'published', 30000);
					console.log('Published Map');
				});
			},
			function()
			{   showElement('mapStatusBadge', 'publishError', 10000);
				console.log('Could not publish map');
			});
		});
    };

	$scope.deleteMap = function()
	{   $scope.map.$remove({id: $scope.mapId}, function()
		{   console.log('Deleted Map');
			$cookieStore.remove("lastMapId");
			$location.path('/maps/');
		});
	};

	function loadMapList(){ $scope.maps = MapList.query(); }

	$scope.toggleAcceptSubmissions = function()
	{   if(!hasUserSubmissionDataset() && $scope.map.accept_submissions)
		{   var newLayer = new LayerList({});
			newLayer.$save(function(layer)
			{   Layer.get({id: layer.id}, function(layer)
				{   layer.name = $scope.map.name + ' User Submissions';
					$scope.map.layers.push(layer.id);
					$scope.map.layer_objects.push(layer);
					$scope.saveMap();
					loadLayerList();
					$scope.addNewDataset(layer, $scope.map.name + ' User Submissions', 'user_submissions');
					$scope.loadMarkerIcons(layer);
					$scope.loadDatasets(layer);
					displayTabForLayerId(layer.id);
				});
			});
		}
    };


// Layers //////////////////////////////////////////////////////////////////////////////////////////////////////////////

	$scope.layersNotInMap = [];
	var layersLoaded = function(){};

	$scope.loadLayers = function ()
	{   $scope.map.layer_objects = [];
		layersLoaded = _.after($scope.map.layers.length, function(){ loadLayerList(); displayTabForLayerId(); });
		_.each($scope.map.layers, function(layerId) { $scope.map.layer_objects.push(loadLayer(layerId)) });
	};

	function loadLayer(layerId)
	{   var layer = Layer.get({id: layerId}, function(layer)
		{   $scope.loadMarkerIcons(layer);
			$scope.loadDatasets(layer);
			layersLoaded();
		});
		return layer;
	};

	function loadLayerList()
	{   $scope.layers = LayerList.query(function()
		{   $scope.layersNotInMap = _.filter($scope.layers,
                                             function(layer){ return !_.contains($scope.map.layers, layer.id) });
		});
	}

	$scope.addLayer = function(layer, name)
	{   // Convert from a LayerList to a Layer object
		Layer.get({id: layer.id}, function(layer)
		{   if(name)
				layer.name = name;
			$scope.map.layers.push(layer.id);
			$scope.map.layer_objects.push(layer);
			$scope.saveMap();
			loadLayerList();
			$scope.loadMarkerIcons(layer);
			$scope.loadDatasets(layer);
			displayTabForLayerId(layer.id);
			return layer;
		});
	};

	$scope.addNewLayer = function(name)
	{   var newLayer = new LayerList({});
		newLayer.$save(function(layer){
			if(name)
				$scope.addLayer(layer, name);
			else
				$scope.addLayer(layer);
		});
	}

	$scope.saveLayer = function(layer)
	{   _.omit(layer, 'dataset_objects').$update({id: layer.id},
			function(){ showElement('layerStatusBadge', 'disablednotice saved', 750) },
			function(){ showElement('layerStatusBadge', 'error') });
		setCookie("lastLayerId", layer.id);
		console.log('Saved Layer');
    };

	$scope.removeLayer = function(layer)
	{   $scope.map.layer_objects = _.without($scope.map.layer_objects, layer);
		$scope.map.layers = _.without($scope.map.layers, layer.id);
		$scope.saveMap();
		loadLayerList();
	};

	// TODO Consider soft deleting db entities
	$scope.deleteLayer = function(layer)
	{   if(!layer)
			var layer = getCurrentLayer();
		layer.$remove({id: layer.id}, function()
		{   console.log('Deleted Layer');
			$scope.removeLayer(layer);
			$cookieStore.remove("lastLayerId");
			showElement('layerStatusBadge', 'deleted');
		});
		$scope.closeDeleteLayerModal();
	};


// Markers /////////////////////////////////////////////////////////////////////////////////////////////////////////////

	$scope.loadMarkerIcons = function (layer)
	{   if(!layer)
			var layer = getCurrentLayer();
		layer.marker_icon_objects = [];

		if(_.size(layer.marker_icons) == 0)
			layer.marker_icons = [1];

		_.each(layer.marker_icons, function(marker_icon)
		{   layer.marker_icon_objects.push(MarkerIcon.get({id: marker_icon}))
		});

		if(!layer.cluster_icon)
			layer.cluster_icon = layer.marker_icons[0];
		layer.cluster_icon_object = MarkerIcon.get({id: layer.cluster_icon});

		if(!layer.legend_icon)
			layer.legend_icon = layer.marker_icons[0];
		layer.legend_icon_object = MarkerIcon.get({id: layer.legend_icon});

		$scope.marker_icons = MarkerIconList.query();
	};

	$scope.addMarker = function(layer, markericon)
	{   layer.marker_icons.push(markericon.id);
		layer.marker_icon_objects.push(markericon);
		$scope.saveLayer(layer);

		if(_.size(layer.marker_icons) == 1)
			$scope.replaceLegendMarker(layer, markericon);
	};

	$scope.saveMarker = function(marker)
	{   marker.$update({id: marker.id});
    };

	$scope.removeMarker = function(markericon, layer)
	{   layer.marker_icon_objects = _.without(layer.marker_icon_objects, markericon);
		layer.marker_icons = _.without(layer.marker_icons, markericon.id);
		$scope.saveLayer(layer);
	};

	$scope.replaceClusterMarker = function(layer, markericon)
	{   layer.cluster_icon = markericon.id;
		layer.cluster_icon_object = markericon;
		$scope.saveLayer(layer);
	};

	$scope.replaceLegendMarker = function(layer, markericon)
	{   layer.legend_icon = markericon.id;
		layer.legend_icon_object = markericon;
		$scope.saveLayer(layer);
	};


// Datasets ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$scope.loadDatasets = function (layer)
	{   layer.dataset_objects = [];
		_.each(layer.datasets, function(dataset){ layer.dataset_objects.push(Dataset.get({id: dataset})) });
		loadDatasetList(layer);
	};

	function loadDatasetList(layer)
	{   $scope.datasets = DatasetList.query(function(datasets)
		{   if(_.size(layer.datasets) > 0)
				layer.datasetsNotInLayer = _.filter(datasets,
                                                    function(dataset){ return !_.contains(layer.datasets, dataset.id) });
			else
				layer.datasetsNotInLayer = datasets;
		});
	}

	$scope.addDataset = function(layer, dataset, name, source_type)
	{   // Convert from a DatasetList to a Dataset object
		Dataset.get({id: dataset.id}, function(dataset)
		{   if(name && source_type)
			{   dataset.name = name;
				dataset.source_type = source_type;
				if (source_type == 'user_submissions')
				{   dataset.latitude_field = 'latitude';
					dataset.longitude_field = 'longitude';
				}
				$scope.saveDataset(dataset);
			}
			layer.datasets.push(dataset.id);
			layer.dataset_objects.push(dataset);
			$scope.saveLayer(layer);
			loadDatasetList(layer);
		});
	};

	$scope.addNewDataset = function(layer, name, source_type)
	{   var newDataset = new DatasetList({});
		newDataset.$save(function(dataset)
		{   if (name && source_type)
				$scope.addDataset(layer, dataset, name, source_type);
			else
				$scope.addDataset(layer, dataset);
		});
	}

	$scope.saveDataset = function(dataset)
	{   dataset.$update({id: dataset.id},
		function(){ showElement('datasetStatusBadge', 'disablednotice saved', 750) },
		function(){ showElement('datasetStatusBadge', 'error') });
    };

	$scope.removeDataset = function(dataset, layer)
	{   layer.dataset_objects = _.without(layer.dataset_objects, dataset);
		layer.datasets = _.without(layer.datasets, dataset.id);
		$scope.saveLayer(layer);
		loadDatasetList(layer);
	};

	$scope.deleteDataset = function(dataset, layer)
	{   if(!dataset)
			var dataset = $scope.dataset;
		if(!layer)
			var layer = getCurrentLayer();
		dataset.$remove({id: dataset.id}, function()
		{   $scope.removeDataset(dataset, layer);
			console.log('Deleted Dataset');
			showElement('datasetStatusBadge', 'deleted');
		});
		$scope.closeDeleteDatasetModal();
	};



// Modals //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$scope.previewModal = {};
	$scope.previewModal.opts = { dialogClass: "modal large", backdropFade: true, dialogFade:true };
	$scope.openPreviewModal = function (){ $scope.previewModal.isOpen = true };
	$scope.closePreviewModal = function (){ $scope.previewModal.isOpen = false };
	$scope.saveAndOpenPreviewModal = function (){ $scope.saveMap(false, true) };

	$scope.markerModal = {};
	$scope.markerModal.opts = { backdropFade: true, dialogFade:true };
	$scope.openMarkerModal = function (){ $scope.markerModal.isOpen = true };
	$scope.closeMarkerModal = function ()
	{   $scope.markerModal.isOpen = false;
	};

	$scope.deleteModalOpts = { dialogClass: "modal", backdropFade: true, dialogFade:true };

	$scope.deleteMapModal = {};
	$scope.openDeleteMapModal = function (){ $scope.deleteMapModal.isOpen = true };
	$scope.closeDeleteMapModal = function (){ $scope.deleteMapModal.isOpen = false };

	$scope.deleteLayerModal = {};
	$scope.closeDeleteLayerModal = function (){ $scope.deleteLayerModal.isOpen = false };
	$scope.openDeleteLayerModal = function (layer)
	{   setCurrentLayer(layer.id);
		$scope.deleteLayerModal.isOpen = true
	};

	$scope.deleteDatasetModal = {};
	$scope.closeDeleteDatasetModal = function (){ $scope.deleteDatasetModal.isOpen = false };
	$scope.openDeleteDatasetModal = function (dataset, layer)
	{   $scope.dataset = dataset;
		setCurrentLayer(layer.id);
		$scope.deleteDatasetModal.isOpen = true
	};



// Marker Uploading ////////////////////////////////////////////////////////////////////////////////////////////////////

	$scope.uploadComplete = function (content, completed)
	{   console.log('UploadComplete');
		console.log('UploadComplete completed: ' + completed);
		console.log('UploadComplete content: ' + content);
		if(completed)
		{   $scope.closeMarkerModal();
			$timeout(function(){ $scope.marker_icons = MarkerIconList.query() }, 3000);
		}
	};
}



// Auxiliary ///////////////////////////////////////////////////////////////////////////////////////////////////////////

function clone(obj)
{   var target = {};
	for(var i in obj)
	{	if(obj.hasOwnProperty(i))
		{	target[i] = obj[i];
		}
	}
	return target;
}
