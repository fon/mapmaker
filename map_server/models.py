# coding=UTF-8

import logging
log = logging.getLogger(__name__)

import os
import csv
from os.path import join
from datetime import datetime, timedelta
from django.db import models
from django.conf import settings
from django.template import Context
from django.utils import simplejson
from django.core.mail import send_mail
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.forms.models import model_to_dict
from django.template.loader import get_template
from map_server.utils import truncate, hashed_uid, slugify
from map_server.utils.transform import british_grid_to_wgs, irish_grid_to_wgs


class Map(models.Model):
    name = models.CharField(max_length=64, default='New Map', unique=True)
    center_latitude = models.DecimalField(decimal_places=7, max_digits=10, default=54.7)
    center_longitude = models.DecimalField(decimal_places=7, max_digits=10, default=-2.0)
    initial_zoom = models.PositiveSmallIntegerField(default=6)
    layers = models.ManyToManyField('Layer', blank=True, null=True)
    tile_set = models.CharField(max_length=32, default='google.maps.MapTypeId.TERRAIN', choices=(('google.maps.MapTypeId.TERRAIN', 'Terrain'), ('google.maps.MapTypeId.ROADMAP', 'Roadmap'),))
    specific_js = models.TextField(blank=True)
    specific_css = models.TextField(blank=True)
    specific_html = models.TextField(blank=True)
    shareable_url_root = models.CharField(max_length=512, blank=True)
    published_url = models.CharField(max_length=512, blank=True)
    accept_submissions = models.BooleanField(default=False)
    submission_header_html = models.TextField(blank=True)

    def __unicode__(self):
        return u'%s' % self.name

    def render(self):
        t = get_template('rendered_maps/template.html')
        rendered_map = t.render(Context({'map': self}))
        return rendered_map

    def preview(self):
        return HttpResponse(self.render())

    def write_to_disk(self):
        filename = '%s.html' % slugify(self.name)
        self.published_url = settings.MAP_PUBLISH_ROOT_URL + filename
        self.save()
        with open(join(settings.MAP_PUBLISH_DIR, filename), 'w+') as f:
            f.write(self.render())

    def get_submissions_layer_name(self):
        for layer in self.layers.all():
            for dataset in layer.datasets.all():
                if dataset.source_type == 'user_submissions':
                    return layer.name


class Layer(models.Model):
    name = models.CharField(max_length=64, default='New Layer')
    datasets = models.ManyToManyField('LayerDataset', blank=True, null=True)
    visible = models.BooleanField(default=False)
    marker_icons = models.ManyToManyField('MarkerIcon', blank=True, null=True)
    legend_icon = models.ForeignKey('MarkerIcon', related_name='layer_legend_icon', blank=True, null=True)
    should_cluster = models.BooleanField(default=True)
    cluster_icon = models.ForeignKey('MarkerIcon', related_name='layer_cluster_icons', blank=True, null=True)
    cluster_text_color = models.CharField(max_length=6, default='000000')  # stored in hex
    z_index = models.PositiveSmallIntegerField(default=0)

    class Meta:
        ordering = ['id']

    def __unicode__(self):
        return u'%s' % self.name


class LayerDataset(models.Model):
    name = models.CharField(max_length=64, default='New Dataset')
    source_location = models.CharField(max_length=64, blank=True)
    source_type = models.CharField(max_length=16, default='fusion', choices=(('fusion', 'Fusion Table'), ('source_code', 'Source Code'), ('json', 'JSON Feed'), ('xml', 'XML Feed'), ('user_submissions', 'User Submissions'),))
    render_location = models.CharField(max_length=16, default='local', choices=(('local', 'Local'), ('remote', 'Remote'),))
    latitude_field = models.CharField(max_length=64, default='lat')
    longitude_field = models.CharField(max_length=64, default='lng')
    decimal_precision = models.PositiveSmallIntegerField(default=3)  # Default of 3 is ~110 meters. http://gis.stackexchange.com/questions/8650/how-to-measure-the-accuracy-of-latitude-and-longitude
    field_override = models.TextField(blank=True)
    fusion_styles = models.TextField(blank=True)
    popup_html = models.TextField(blank=True)
    parser = models.TextField(blank=True)
    max_result_count = models.PositiveSmallIntegerField(blank=True, null=True)
    render_frequency_in_minutes = models.PositiveSmallIntegerField(default=30) # TODO add so cronjob runs every minute and only renders when appropriate
    last_render_time = models.DateTimeField(default=datetime.now)
    render_status = models.CharField(max_length=16, default='waiting', choices=(('waiting', 'Waiting'), ('running', 'Running'), ('failed', 'Failed'),))

    def __unicode__(self):
        if self.name:
            return u'%s' % self.name
        elif self.id:
            return u'Data Source %s' % self.id
        else:
            return u'New Data Source'

    def save(self, *args, **kwargs):
        self.popup_html = self.popup_html.replace('\n', '')
        # if self.source_type == 'user_submissions' and not self.source_location:
        #     self.source_location = reverse('approved_submissions_feed', id=map.id)
        # If source_type is a source code and the feed doesn't exist, write it out for the first time
        if self.source_type == 'source_code' and self.source_location and not os.path.exists(self.get_filename()):
            self.write_feed()
        super(LayerDataset, self).save(*args, **kwargs)

    def get_filename(self):
        return '%s.json' % slugify(self.name)

    def get_feed_url(self):
        return settings.FEED_URL + self.name + '.json'

    def write_feed(self):
        with open(join(settings.FEED_OUTPUT_DIR, self.get_filename()), 'w+') as f:
            json = self.get_source_code_feed_data()
            # Check for < 50% filesize difference (yes, just compare string lengths)
            if len(json) > len(f.read())/2:
                f.write(json)

    def generate_feed(self):
        max_hours_to_reset = 6
        if datetime.now() - self.last_render_time > timedelta(hours=max_hours_to_reset):
            # Force a reset if it's been over max_hours_to_reset hours
            log.error('Reset %s feed generation.  Has stalled for %s hours.' % (self.name, max_hours_to_reset))
            self.render_status = 'waiting'
            self.save()
        if self.render_status == 'waiting' and datetime.now() - self.last_render_time > timedelta(minutes=self.render_frequency_in_minutes):
            log.debug('Rendering %s feed.' % self.name)
            self.render_status = 'running'
            self.save()
            try:
                self.write_feed()
                self.last_render_time = datetime.now()
                self.render_status = 'waiting'
                log.info('%s rendered to %s at %s. Next render in %s minutes' %
                         (self.name, self.get_filename(), self.last_render_time, self.render_frequency_in_minutes))
            except Exception as e:
                self.render_status = 'failed'
                log.error('Render failed for dataset %s (id:%s):\n  %s' % (self.name, self.id, e))
            self.save()
        else:
            log.debug('Skipping render for %s. Last render at %s, frequency set to %s, current status of %s' %
                      (self.name, self.last_render_time, self.render_frequency_in_minutes, self.render_status))


class MarkerIcon(models.Model):
    name = models.CharField(max_length=64, blank=True)
    icon = models.FileField(upload_to='markers', null=True, blank=True)

    def __unicode__(self):
        if self.name:
            return u'%s' % self.name
        elif self.id:
            return u'Marker %s' % self.id
        else:
            return u'New Marker'


class UserSubmission(models.Model):
    map = models.ForeignKey(Map)
    photo = models.FileField(upload_to='uploaded', null=True, blank=True)  # TODO: Consider ImageField and requiring Pillow if PIL will build easily on SUSE
    postcode = models.CharField(max_length=8, blank=True)  # Should form validate one of postcode or lat/lng pair is submitted?
    # latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    # longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    firstname = models.CharField(blank=True, max_length=64)
    lastname = models.CharField(blank=True, max_length=64)
    email = models.CharField(blank=True, max_length=64)
    description = models.TextField(blank=True)
    data_protection_can_send = models.BooleanField(default=False)
    color = models.CharField(max_length=6, blank=True)  # stored in hex
    uid = models.CharField(blank=True, max_length=64)
    approval_status = models.CharField(max_length=16, default='pending',
                                       choices=(('pending', 'Pending'), ('accepted', 'Accepted'), ('rejected', 'Rejected'),))
    approval_email_sent = models.BooleanField(default=False)
    geocoding_attempts = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name = "User Submitted Photo"

    def save(self, *args, **kwargs):
        if self.approval_status == 'accepted' and self.email and not self.approval_email_sent:
            try:
                pass
                # TODO refactor post approval email to map specific code
                # send_mail(subject, message, from_email, recipient_list, fail_silently=False, auth_user=None, auth_password=None, connection=None, html_message=None)¶
                # self.approval_email_sent = True
            except:
                log.critical('Could not send submission approval e-mail. Email template not set?')
        super(UserSubmission, self).save(*args, **kwargs)
        if not self.uid:
            self.uid = hashed_uid(self.id)
        super(UserSubmission, self).save(*args, **kwargs)

    def geocode(self):
        pass

    @classmethod
    def write_data_as_csv(cls, response):
        writer = csv.writer(response)
        fields = cls._meta.get_all_field_names()
        writer.writerow(fields)

        for submission in cls.objects.all():
            submission, row = model_to_dict(submission), []
            for field in fields:
                row.append(submission[field])
            writer.writerow(row)
        return response


class ExclusionBox(models.Model):
    """To specify parts of the map to exclude markers from. e.g. preventing flowers in the sea off Scilly."""
    lat_1 = models.DecimalField(decimal_places=5, max_digits=7)
    lon_1 = models.DecimalField(decimal_places=5, max_digits=8)
    lat_2 = models.DecimalField(decimal_places=5, max_digits=7)
    lon_2 = models.DecimalField(decimal_places=5, max_digits=8)

    class Meta:
        verbose_name_plural = 'Exclusion Boxes'


class MapDataSource(models.Model):
    """To interface with data already in contacts_extra"""
    x_coord = models.CharField(max_length=6)
    y_coord = models.CharField(max_length=7)
    source_code = models.CharField(max_length=31)
    postcode = models.CharField(max_length=8)  # Get Northern Ireland postcodes to check for BT
    action_id = models.PositiveIntegerField(primary_key=True)  # Not really a primary key, but primary_key has to be set or an id field will be expected
    # donation_id = models.PositiveIntegerField(null=True)
    dated = models.DateTimeField(null=True, )     #datetime2(3)  # WTH is datetime2?

    class Meta:
        managed = False  # instructs Django to not create this table i.e. use existing table
        db_table = 'action_master_for_maps'
        ordering = ["-dated"]  # Get most recent by default

    def save(self, *args, **kwargs):
        pass  # Extra safeguard



