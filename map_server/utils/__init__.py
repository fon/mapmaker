# coding=UTF-8

import logging
log = logging.getLogger(__name__)

import re
import requests
import hashlib
import unicodedata
from django.conf import settings
from django.utils.safestring import mark_safe


def truncate(s, d='.', n=1, l=3):
    # TODO: refactor to utils
    "Returns s truncated at the n'th occurrence plus l chars after the delimiter, d."
    # Lat long precision: http://gis.stackexchange.com/a/8674
    # TODO: Clear up edge cases
    splitstring = str(s).split(d)
    splitstring[n] = splitstring[n][:l]
    return d.join(splitstring)


def hashed_uid(id):
    salt = settings.SECRET_KEY
    return hashlib.sha256(salt + str(id)).hexdigest()


# Method curiously missing from django.utils... 1.4 issue?
def slugify(value):
    """
    Converts to lowercase, removes non-word characters (alphanumerics and
    underscores) and converts spaces to hyphens. Also strips leading and
    trailing whitespace.
    """

    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return mark_safe(re.sub('[-\s]+', '-', value))


def extractPostcode(string):
    try:
        return re.findall(r'[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][A-Z]{2}', string)[0]
    except Exception as e:
        log.error('Could not extract postcode from: %s\n  %s' % (string, e))
        return ''


def geocodePostcode(postcode, geocode_url=settings.GEOCODE_URL):
    postcode_nospaces = postcode.replace(" ", "")
    geocode_result = requests.get(geocode_url + postcode_nospaces, timeout=10).json()

    try:
        return {'lat': geocode_result['wgs84_lat'], 'lng': geocode_result['wgs84_lon']}
    except KeyError as e:
        log.debug('Postcode %s geocoding failed: %s' % (postcode, e))
        return None
