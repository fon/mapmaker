MapMaker
========
MapMaker is an open source server and web app for creating standalone publishable maps.  It uses Django on the backend and AngularJS for the map editing interface:

* Google Maps is used as the base map
* It is overlaid with toggleable layers
    - Each layer contains one or more datasets
    - Each dataset is backed by a JSON, Fusion table, or XML datasource
    - Points on a layer can be clustered and have custom icons
* Maps can be styled with extra
* Maps can accept user submissions


Code location:
https://bitbucket.org/mapmaker/mapmaker

IRC:
irc.freenode.net #foeewni

Prerequisites
-------------
* Python 2.7
* Virtualenv


Development Setup
-----------------

### Checkout Repo

    git clone https://bitbucket.org/mapmaker/mapmaker.git

### Activate virtualenv and install requirements

    cd checkout_dir
    virtualenv env

If you want to make your own custom settings, you can copy `devel.py` to e.g.
`devel_yourname.py` and then change this export appropriately. If you have
virtualenv-wrapper installed, you can do this:

    echo "export DJANGO_SETTINGS_MODULE=map_server.settings.devel" >> env/bin/postactivate

If you haven't got virtualenv-wrapper installed (you should do!) then you can
just use the `--settings` option after any `manage.py` commands, or run the 
`export` command in your shell. Examples below (obviously the `manage.py`
commands will only run after you've done the install bits):

    DJANGO_SETTINGS_MODULE=map_server.settings.devel ./manage.py runserver
    ./manage.py runserver --settings=map_server.settings.devel
    export DJANGO_SETTINGS_MODULE=map_server.settings.devel

Activate the env and install the requirements:

    . env/bin/activate
    pip install -r requirements.txt


### Run syncdb and South migrate

    ./manage.py syncdb
    ./manage.py migrate map_server

Create a superuser when prompted

### Load data from fixtures

    ./manage.py runscript map_server.fixtures.maps
    ./manage.py runscript map_server.fixtures.users
    ./manage.py loaddata exclusion_boxes


Change passwords for `administrator` and `moderator` users via admin

### Run development server
    ./manage.py runserver 0.0.0.0:8000


Contribute
----------
We love contributions.  If you know Python, Javascript, Django, Angular, Javascript please do contribute code:

- Check for open issues or open a fresh issue to start a discussion around a feature idea or a bug.
- Fork this repository on BitBucket.  Branch off _develop_ to make your changes.
- Write a test which shows the feature works or the bug was fixed.
- Send a pull request on BitBucket; they're reviewed every Wednesday night.  Oh, and make sure to add yourself to AUTHORS.txt.

If you're not a coder, you can still really help us.  We love contributions to documentation and artwork.

We have frequent meetups: http://www.meetup.com/London-Climate-Change-Coders/


OS X Notes
----------
If pip install throws an error on OS X 10.9.2

    ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future pip install pyproj==1.9.3 lxml==3.3.5

then complete environment install as usual

    pip install -r requirements.txt

Debian/Ubuntu Notes
-------------------
`sudo apt-get install python-dev python-pip python-virtualenv libxml2-devel libxslt-devel`

Roadmap
-------
Moved to Bitbucket Issues: https://bitbucket.org/foeewni/mapmaker/issues?status=new&status=open

License
-------
MapMaker is licensed under the Affero GPL (AGPL) License.  See LICENSE.txt.